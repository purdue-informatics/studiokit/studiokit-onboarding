﻿namespace StudioKit.Onboarding
{
	public static class OnboardingConstants
	{
		// I2A2 Characteristics from https://www.purdue.edu/apps/account/html/chars.txt

		// EmployeeGroup codes
		public const string AdminProfessionalStaff = "13101";
		public const string Clerical = "13102";
		public const string ClinicalResearch = "13103";
		public const string ContinuingLecturer = "13104";
		public const string Faculty = "13105";
		public const string FellowshipPostDoctoral = "13106";
		public const string FellowshipPreDoctoralOther = "13107";
		public const string LimitedTermLecturer = "13111";
		public const string ManagementProfessional = "13112";
		public const string OperationsAssistant = "13115";
		public const string PostDocsInternsResidents = "13118";
		public const string VisitingOrEmeritusFaculty = "13124";

		// Job codes from https://www.purdue.edu/apps/account/html/chars.txt
		public const string GraduateLecturer = "10360";
		public const string GraduateResearchAssistant = "10361";
		public const string GraduateTeachingAssistant = "10362";
	}
}